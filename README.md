# Configuração da API

### Dependências

* Python 3.7
    * Pacote flask (pip install flask -- para instalar)
    * Pacote flask_cors (pip install flask -- para instalar)

Após instalar os pacotes acima, prosseguir com o seguinte:

1. Na pasta root do projeto, digite e rode no terminar: python ./api/api.py

2. Abra o documento HTML no navegador que desejar.

### Regras para cadastro da participação

1. Todos os campos são obrigatórios.
2. No campo de `participation` serão aceitos apenas números de 1 a 100.
3. Se a soma do valor do campo `participation` com os outros valores já cadastrados resultar em mais que 100, o item não será cadastrado.