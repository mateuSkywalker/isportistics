#!flask/bin/python
from flask import Flask, jsonify, _app_ctx_stack, request, json, abort
from flask_cors import CORS

import sqlite3

app = Flask(__name__)
CORS(app)

members = []

@app.route('/get', methods=['GET'])
def get_tasks():
    return jsonify({'members': members})

@app.route('/insert', methods=['POST'])
def insert_task():
    member = request.form

    name = member.getlist('firstName')
    lastName = member.getlist('lastName')
    participation = member.getlist('participation')
    
    if (name == '') or (lastName == '') or (participation == ''):
        print('Invalid Data')
        abort(400)

    members.append(member)
    response = app.response_class(response=json.dumps(members[-1]), status=200, mimetype='application/json')
    print(members)
    return response

if __name__ == '__main__':
    app.run(debug=True)