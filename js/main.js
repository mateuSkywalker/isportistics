(async() => {
    const ajax = new Ajax();
    const table = document.querySelector('.app__table-body');
    let form = document.querySelector('.app__form');
    const formButton = document.querySelector('.app__button');
    const formItems = document.querySelectorAll('.app__input');
    const persons = document.querySelector('.app__persons');
    
    const data = await (await ajax.get()).json();
    let members = data.members;
    let i = 1;

    let currentParticipation = 0;

    let membersLength = members.length;
    let partsOncChart = [];
    let colorsOnChart = [];
    

    init();
        
    // handle submit
    formButton.addEventListener('click', async($event) => {
        const name = formItems[0].value;
        const lastName = formItems[1].value;
        const participation = formItems[2].value;

        const isParticipationValid = validateParticipation(participation)

        if (name !== '' && lastName !== '' && participation !== '' && isParticipationValid) {
            const newItem = {
                name, 
                lastName,
                participation
            };
            
            const post = await ajax.post(newItem);
            
            if(post.status === 200) {
                const newMember = await post.json();
                const nameNewMember = `${newMember.firstName} ${newMember.lastName}`;
                members.push(newMember);

                
                createTableItems(newMember);
                form.reset();
                
                membersLength++;
                colorsOnChart.push(addColor())
                partsOncChart.push(newMember.participation)

                drawChart(membersLength, partsOncChart, colorsOnChart);
                renderNames(nameNewMember, colorsOnChart[colorsOnChart.length - 1]);
            }    
        }
    
    });

    function init() {
        members.forEach((element, i) => {
            createTableItems(element)
            partsOncChart.push(element.participation);
            colorsOnChart.push(addColor())
    
            const name = `${element.firstName} ${element.lastName}`;
            renderNames(name, colorsOnChart[i]);
        });
    
        drawChart(membersLength, partsOncChart, colorsOnChart);
    }

    function createTableItems(member) {
        const newItem = document.createElement('tr');
        const thNumber = document.createElement('th');
        const tdName = document.createElement('td');
        const tdLast = document.createElement('td');
        const tdPart = document.createElement('td');            
        
        thNumber.innerHTML = `${i}`;
        tdName.innerHTML = member.firstName;
        tdLast.innerHTML = member.lastName;
        tdPart.innerHTML = member.participation + '%';
        
        currentParticipation += Number(member.participation);
        
        newItem.appendChild(thNumber);
        newItem.appendChild(tdName);
        newItem.appendChild(tdLast);
        newItem.appendChild(tdPart);
        table.appendChild(newItem);
        i++;
    }

    function drawChart(numberOfParts, parts, colorsOnChart) {
        const data = 
        {
            numberOfParts,
            parts:{"pt": parts},
            colors:{"cs": colorsOnChart}
        };

        const chart = new chartGraph();
        chart.set(150, 150, 100, 0, Math.PI*2, 60, "#fff");
        chart.draw(data);
    }

    function renderNames(name, color) {
        const box = document.createElement('div');
        const nameElement = document.createElement('div');
        const personContainer = document.createElement('div');
        personContainer.classList.add('d-flex');
        personContainer.classList.add('justify-content-center');
        personContainer.classList.add('mt-4');

        box.classList.add('app__box');
        box.style.background = color;

        nameElement.innerHTML = name;
        nameElement.style.color = color;
        nameElement.classList.add('app__name')
        nameElement.classList.add('ml-3');


        personContainer.appendChild(box);
        personContainer.appendChild(nameElement);
        
        persons.appendChild(personContainer);
    }

    function validateParticipation(part) {
        if(part >= 1 && part <= 100 && (Number(part) + currentParticipation) <= 100) {
            return true
        }
        return false;
    }

    function addColor() {
        return CSS_COLOR_NAMES[Math.floor(Math.random() * CSS_COLOR_NAMES.length-1) + 1]
    }
})();