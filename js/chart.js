var canvas  = document.querySelector(".app__canvas");
var chart = canvas.getContext("2d");

function chartGraph() {

    this.x , this.y , this.radius , this.lineWidth , this.strockStyle , this.from , this.to = null;
    
    this.set = function( x, y, radius, from, to, lineWidth, strockStyle) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.from=from;
        this.to= to;
        this.lineWidth = lineWidth;
        this.strockStyle = strockStyle; 
    }

    this.draw = function(data) {
        chart.beginPath();
        chart.lineWidth = this.lineWidth;
        chart.strokeStyle = this.strockStyle;
        chart.arc(this.x , this.y , this.radius , this.from , this.to);
        chart.stroke();
        
        const numberOfParts = data.numberOfParts;
        const parts = data.parts.pt;
        const colors = data.colors.cs;
        let startAngle = 0;
        for(let i = 0; i<numberOfParts; i++) {
            let mediaAngle = (startAngle + (startAngle + (Math.PI * 2) * (parts[i] / 100))) / 2;
            chart.beginPath();
            chart.strokeStyle = colors[i];
            chart.arc(200 + Math.cos(mediaAngle) * 5 , 200 + Math.sin(mediaAngle) * 5, this.radius, startAngle, startAngle + (Math.PI * 2) * (parts[i] / 100));
            chart.stroke();
            startAngle += (Math.PI * 2) * (parts[i] / 100);
        }
    }
}
