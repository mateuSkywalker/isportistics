API = 'http://127.0.0.1:5000'
class Ajax {

    
    get() {
        return fetch(`${API}/get`, { method: 'GET'})
    }
    
    post(data) {
        let formData = new FormData(); 
    
        formData.append('firstName', data.name);
        formData.append('lastName', data.lastName);
        formData.append('participation', Number(data.participation));

        return fetch(`${API}/insert`, { method: 'POST', body: formData })
    }
}